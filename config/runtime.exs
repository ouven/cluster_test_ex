import Config

get_env! = fn key -> System.get_env(key) || raise("#{key} is not set") end
get_env = fn key, default -> System.get_env(key) || default end

case config_env() do
  :prod ->
    config :libcluster,
      topologies: [
        cluster_test: [
          strategy: Cluster.Strategy.Kubernetes,
          config: [
            kubernetes_node_basename: "cluster-test",
            kubernetes_selector: "app=cluster-test",
            kubernetes_namespace: get_env!.("NAMESPACE"),
            polling_interval: 10_000
          ]
        ]
      ]

  _ ->
    nil
end
