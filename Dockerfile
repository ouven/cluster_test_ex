FROM hexpm/elixir-amd64:1.12.1-erlang-24.0.2-debian-buster-20210326 as build

ENV MIX_ENV=prod
ENV LANG=C.UTF-8

WORKDIR /build
ADD . .

RUN mix local.hex --force
RUN mix local.rebar --force
RUN mix deps.get

RUN mix compile
RUN mix release


#=================
# deployment Stage
#=================
FROM debian:buster-slim as runtime
ENV LANG=C.UTF-8
ENV HOME=/opt/app

#Create /opt/app directory and default user \
RUN apt update && apt install -y \
    ca-certificates \
    && update-ca-certificates --fresh \
    && mkdir -p ${HOME} \
    && chown -R nobody: ${HOME} \
    && rm -rf /var/lib/{apt,dpkg,cache,log}

WORKDIR ${HOME}

# Set environment variables and expose port

# http api
EXPOSE 4000
# metrix
EXPOSE 4001
USER nobody

#Copy and extract .tar.gz Release file from the previous stage
COPY --from=build --chown=nobody:nogroup /build/_build/prod/rel/default .

#Set default entrypoint and command
ENTRYPOINT ["/opt/app/bin/default"]
CMD ["start"]
